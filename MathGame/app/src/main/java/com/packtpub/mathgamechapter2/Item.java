package com.packtpub.mathgamechapter2;

import org.json.JSONObject;

/**
 * Created by SPanin on 6/23/2016.
 */
class Item {
    private String m_name = null;
    private int m_value  = 0;

    public Item() {
    }
    public Item(String name, int value) {
        m_name = name;
        m_value = value;
    }
    void setName(String name) {
        m_name = name;
    }
    String name() {
        return m_name;
    }
    void setValue(int value) {
        m_value = value;
    }
    int value() {
        return m_value;
    }
    public static Item fromJson(String s) {
        Item result = new Item();

        try {
            JSONObject o = new JSONObject(s);
            result.setName(o.get("name").toString());
            result.setValue(Integer.parseInt(o.get("value").toString()));
        } catch(Exception e) {
        }

        return result;
    }
};