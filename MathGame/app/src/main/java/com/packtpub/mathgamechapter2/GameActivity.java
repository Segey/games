package com.packtpub.mathgamechapter2;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.Vector;

enum OperandType {Multiplication, Plus, Minus, Division};
class Operand {
    static OperandType next() {
        int value = new Random().nextInt(OperandType.values().length);
        return OperandType.values()[value];
    }
}
class InitValues {
    private int m_val1 = 0;
    private int m_val2 = 0;

    InitValues(int val1, int val2) {
        m_val1= val1;
        m_val2 = val2;
    }
    static InitValues next(int max) {
        Random r = new Random();
        final int val1 = r.nextInt(max);
        final int val2 = r.nextInt(max);
        return new InitValues(val1, val2);
    }
    void setVal1(int val) {
        m_val1 = val;
    }
    void setVal2(int val) {
        m_val2 = val;
    }
    int val1() {
        return m_val1;
    }
    int val2() {
        return m_val2;
    }
}
class ResultValues {
    private int m_valid = 0;
    private int m_wrong1 = 0;
    private int m_wrong2 = 0;

    private static int getResult(InitValues values, OperandType type) {
        if(type == OperandType.Division) {
                if(values.val2() == 0) return Integer.MAX_VALUE;
                return values.val1() / values.val2();
            }
        if(type == OperandType.Minus) return values.val1() - values.val2();
        if(type == OperandType.Plus) return values.val1() + values.val2();
        return values.val1() * values.val2();
    }
    ResultValues(int valid, int wrong1, int wrong2) {
        m_valid = valid;
        m_wrong1 = wrong1;
        m_wrong2 = wrong2;
    }
    int valid() {
        return m_valid;
    }
    int wrong1() {
        return m_wrong1;
    }
    int wrong2() {
        return m_wrong2;
    }
    static ResultValues next(int max, InitValues values, OperandType type) {
        Random rand = new Random();
        final int correct = getResult(values, type);
        int wrong1 = -1;

        for(int i = 0; i != 10; ++i) {
            int result = rand.nextInt(max * 2);
            if(correct != result) {
                if(wrong1 == -1) wrong1 = result;
                else {
                    if(result != wrong1)
                        return new ResultValues(correct, wrong1, result);
                }
            }
        }
        return new ResultValues(correct, correct - 1, correct + 1);
    }
}
class PrefValueMaker {
    public final static String makeScoreValue(String name, int score) {
        return String.format("{\"id\": %d, \"name\": \"%s\", \"value\":%d}", System.currentTimeMillis(), name, score);
    }
}
class Pref {
    private static SharedPreferences m_prefs = null;

    static class MinValue {
        private static int m_min = -1;
        public static final int get() {
            if(m_min == -1)
                m_min = m_prefs.getInt("min", 0);
            return m_min;
        }
        public static final void set(int val) {
            m_min = val;
            SharedPreferences.Editor ed = m_prefs.edit();
            ed.putInt("min", m_min);
            ed.apply();
        }
    }
    private Pair<Integer, Integer> findMin(Vector<String> vec) {
        int min = Integer.MAX_VALUE, index = -1;
        for(int i = 0; i != vec.size(); ++i) {
            Item item = Item.fromJson(vec.get(i));
            if(item.value() < min) {
                min = item.value();
                index = i;
            }
        }
        return new Pair<Integer, Integer>(min, index);
    }
    private void saveScore(Set<String> list) {
        SharedPreferences.Editor ed = m_prefs.edit();
        ed.putStringSet("score", list);
        ed.commit();
    }
    Pref(Context context) {
        m_prefs = context.getSharedPreferences("MathGame", context.MODE_PRIVATE);
    }
    public final void save(String name, int score) {
        if(MinValue.get() >= score) return;

        Set<String> list = new HashSet<String>();
        list = m_prefs.getStringSet("score", list);

        Vector<String> vec = new Vector<String>(list);
        Pair<Integer, Integer> pair = findMin(vec);

        if(0 <= pair.second && pair.second <= 10)
            vec.set(pair.second, PrefValueMaker.makeScoreValue(name, score));

        saveScore(new HashSet<String>(vec));
        pair = findMin(vec);
        MinValue.set(pair.first);
    }
    public final boolean isValuesExists() {
        return m_prefs.contains("score")
            && m_prefs.contains("min")
         ;
    }
    public final void saveScoreSet(Set<String> list, int min) {
        saveScore(list);
        MinValue.set(min);
    }
};
public class GameActivity extends Activity implements Button.OnClickListener {
    private Button btnChoice1  = null;
    private Button btnChoice2  = null;
    private Button btnChoice3  = null;
    private TextView textPartA = null;
    private TextView textPartB = null;
    private TextView textScore = null;
    private TextView textLevel = null;
    private TextView textOperator = null;
    private int m_score = 0;
    private int m_level = 1;
    private int m_current  = 0;
    private Pref m_pref = null;

    private void init() {
        btnChoice1 = (Button)findViewById(R.id.btnChoice1);
        btnChoice2 = (Button)findViewById(R.id.btnChoice2);
        btnChoice3 = (Button)findViewById(R.id.btnChoice3);
        textPartA = (TextView)findViewById(R.id.textPartA);
        textPartB = (TextView)findViewById(R.id.textPartB);
        textOperator = (TextView)findViewById(R.id.textOperator);
        textScore = (TextView)findViewById(R.id.score);
        textLevel = (TextView)findViewById(R.id.level);

        btnChoice1.setOnClickListener(this);
        btnChoice2.setOnClickListener(this);
        btnChoice3.setOnClickListener(this);
        m_pref = new Pref(getApplicationContext());
    }
    private final void initResultMap() {
        if(m_pref.isValuesExists()) return;

        Set<String> list = new HashSet<String>(10);
        for(int i = 1; i != 11; ++i) {
            final int val = 110 - i * 10;
            list.add(PrefValueMaker.makeScoreValue("Sergey", val));
        }

        m_pref.saveScoreSet(list, 10);
    }
    private void setOperatorText(OperandType type) {
        if(type == OperandType.Division) textOperator.setText("/");
        else if(type == OperandType.Minus) textOperator.setText("-");
        else if(type == OperandType.Plus) textOperator.setText("+");
        else textOperator.setText("*");
    }
    private void setValues(ResultValues values) {
        Random rand = new Random();
        final int val = rand.nextInt(3);
        final String text = m_current == Integer.MAX_VALUE ? "Ошибка": "" + values.valid();

        if(val == 0) {
            btnChoice1.setText(text);
            btnChoice2.setText("" + values.wrong1());
            btnChoice3.setText("" + values.wrong2());
        }
        else if(val == 1) {
            btnChoice1.setText("" + values.wrong1());
            btnChoice2.setText(text);
            btnChoice3.setText("" + values.wrong2());
        }
        else {
            btnChoice1.setText("" + values.wrong1());
            btnChoice2.setText("" + values.wrong2());
            btnChoice3.setText(text);
        }
    }
    private void nextStep() {
        OperandType type = Operand.next();
        InitValues values = InitValues.next(m_level * 10);
        ResultValues result_values = ResultValues.next(m_level * 10, values, type);
        m_current = result_values.valid();

        setValues(result_values);
        setOperatorText(type);
        textPartA.setText("" + values.val1());
        textPartB.setText("" + values.val2());
        textLevel.setText(String.format("Уровень: %d", m_level));
        textScore.setText(String.format("Очки: %d", m_score));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        init();
        initResultMap();
        nextStep();
    }

    @Override
    public void onClick(View view) {
        final CharSequence text = ((Button) view).getText();
        final int val = text.equals("Ошибка") ? Integer.MAX_VALUE : Integer.parseInt("" + text);
        final String answer = val == m_current ? "Правильно" : "Не верно";
        Toast.makeText(getApplicationContext(), answer, Toast.LENGTH_SHORT).show();
        if (val == m_current) {
            ++m_score;
            if (m_score % 10 == 0) ++m_level;
        } else {
            m_pref.save("Sergey", m_score);
            m_score = 0;
            m_level = 1;
        }
        nextStep();
    }
}
