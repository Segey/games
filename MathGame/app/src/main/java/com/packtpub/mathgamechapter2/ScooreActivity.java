package com.packtpub.mathgamechapter2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

public class ScooreActivity extends Activity {
    private GridView m_view = null;

    private final void init() {
        m_view = (GridView)findViewById(R.id.m_view);
    }
    private final ArrayList<String> makeArrayList(TreeMap<Integer, Vector<Item>> map) {
        ArrayList<String> result = new ArrayList<>();

        int cx = 0;
        for(Map.Entry<Integer, Vector<Item>> entry: map.entrySet()) {
            Vector<Item> vec = entry.getValue();
            for(Item i: vec) {
                result.add(String.format("%d.", ++cx));
                result.add(i.name());
                result.add("" + i.value());
            }
        }
        return result;
    }
    private final TreeMap<Integer, Vector<Item>> readItems() {
        SharedPreferences prefs = getSharedPreferences("MathGame", MODE_PRIVATE);

        Set<String> list = new HashSet<String>();
        list = prefs.getStringSet("score", list);

        TreeMap<Integer, Vector<Item>> map = new TreeMap<Integer, Vector<Item>>(Collections.reverseOrder());

        for (String s: list) {
            Item item = Item.fromJson(s);

            Vector<Item> vec = map.get(item.value());
            if (vec == null) vec = new Vector<Item>();
            vec.add(item);
            map.put(item.value(), vec);
        }
        return map;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scoore);
        init();

        ArrayList<String> list = makeArrayList(readItems());
        if(list.size() > 29)
        m_view.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, list.subList(0,30)));
    }
}
