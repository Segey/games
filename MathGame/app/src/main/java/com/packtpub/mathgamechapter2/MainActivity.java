package com.packtpub.mathgamechapter2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends Activity implements Button.OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((Button)findViewById(R.id.btnPlay)).setOnClickListener(this);
        ((Button)findViewById(R.id.btnScores)).setOnClickListener(this);
        ((Button)findViewById(R.id.btnExit)).setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btnPlay) startActivity(new Intent(getApplicationContext(), GameActivity.class));
        else if(view.getId() == R.id.btnScores) startActivity(new Intent(getApplicationContext(), ScooreActivity.class));
        else {
            finish();
            System.exit(0);
        }
    }
}
