package the.perfect.memorygame;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
    private Button m_play = null;
    public static int hiScore = 0;
    SharedPreferences prefs = null;
    String dataName = "MyData";
    String intName = "MyInt";
    int defaultInt = 0;

    private final void init() {
        setContentView(R.layout.activity_main);

        prefs = getSharedPreferences(dataName,MODE_PRIVATE);
        hiScore = prefs.getInt(intName, defaultInt);

        TextView score = (TextView)findViewById(R.id.m_record);
        score.setText("Рекорд: " + hiScore);

        m_play = (Button)findViewById(R.id.m_play);
        m_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), GameActivity.class));
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
    }
}
