package the.perfect.memorygame;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GameActivity extends Activity implements View.OnClickListener {
    private SoundPool m_sound_pool = null;
    int sample1 = -1;
    int sample2 = -1;
    int sample3 = -1;
    int sample4 = -1;
    private TextView m_score = null;
    private TextView m_level = null;
    private TextView m_go = null;
    private Button m_but1 = null;
    private Button m_but2 = null;
    private Button m_but3 = null;
    private Button m_but4 = null;
    private Button m_replay = null;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String dataName = "MyData";
    String intName = "MyInt";
    int defaultInt = 0;
    int hiScore;
    Animation wobble;

    int difficultyLevel = 3;                //Some variables for our thread
    int[] sequenceToCopy = new int[100];  //An array to hold the randomly generated sequence

    private Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (playSequence) {
                switch (sequenceToCopy[elementToPlay]) {
                    case 1:
                        m_but1.startAnimation(wobble);
                        m_sound_pool.play(sample1, 1, 1, 0, 0, 1);
                        break;
                    case 2:
                        m_but2.startAnimation(wobble);
                        m_sound_pool.play(sample2, 1, 1, 0, 0, 1);
                        break;
                    case 3:
                        m_but3.startAnimation(wobble);
                        m_sound_pool.play(sample3, 1, 1, 0, 0, 1);
                        break;
                    case 4:
                        m_but4.startAnimation(wobble);
                        m_sound_pool.play(sample4, 1, 1, 0, 0, 1);
                        break;
                }
                elementToPlay++;
                if (elementToPlay == difficultyLevel)
                    sequenceFinished();
                myHandler.sendEmptyMessageDelayed(0, 1500);
            }
        }
    };

    boolean playSequence = false;
    int elementToPlay = 0;
    int playerResponses;
    int playerScore;
    boolean isResponding;

    private int initSample(int index) throws Exception {
        AssetManager assetManager = getAssets();    //Create objects of the 2 required classes
        AssetFileDescriptor descriptor = assetManager.openFd(String.format("Files/Audio/sample%d.wav", index));    //create our three fx in memory ready for use
        return m_sound_pool.load(descriptor, 0);
    }

    private void init() {
        m_sound_pool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);

        try {
            sample1 = initSample(1);
            sample2 = initSample(2);
            sample3 = initSample(3);
            sample4 = initSample(4);
        } catch (Exception e) {
        }
        m_score = (TextView) (findViewById(R.id.m_score));
        m_level = (TextView) (findViewById(R.id.m_level));
        m_go = (TextView) (findViewById(R.id.m_go));
        m_but1 = (Button) (findViewById(R.id.m_but1));
        m_but2 = (Button) (findViewById(R.id.m_but2));
        m_but3 = (Button) (findViewById(R.id.m_but3));
        m_but4 = (Button) (findViewById(R.id.m_but4));
        m_replay = (Button) (findViewById(R.id.m_replay));

        m_but1.setOnClickListener(this);
        m_but2.setOnClickListener(this);
        m_but3.setOnClickListener(this);
        m_but4.setOnClickListener(this);
        m_replay.setOnClickListener(this);

        prefs = getSharedPreferences(dataName,MODE_PRIVATE);
        editor = prefs.edit();
        hiScore = prefs.getInt(intName, defaultInt);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        wobble = AnimationUtils.loadAnimation(this, R.anim.wobble);
        init();
         playASequence();
    }

    @Override
    public void onClick(View view) {
        if (!playSequence) {
            switch (view.getId()) {
                case R.id.m_but1:
                    m_sound_pool.play(sample1, 1, 1, 0, 0, 1);
                    checkElement(1);
                    break;
                case R.id.m_but2:
                    m_sound_pool.play(sample2, 1, 1, 0, 0, 1);
                    checkElement(2);
                    break;
                case R.id.m_but3:
                    m_sound_pool.play(sample3, 1, 1, 0, 0, 1);
                    checkElement(3);
                    break;
                case R.id.m_but4:
                    m_sound_pool.play(sample4, 1, 1, 0, 0, 1);
                    checkElement(4);
                    break;
                case R.id.m_replay:
                    difficultyLevel = 3;
                    playerScore = 0;
                    m_score.setText("Очки: " + playerScore);
                    playASequence();
                    break;
            }
        }
    }

    public final void createSequence() {
        int ourRandom;
        for (int i = 0; i != difficultyLevel; ++i) {
            ourRandom = new Random().nextInt(4);
            ++ourRandom;
            sequenceToCopy[i] = ourRandom;
        }
    }

    public void playASequence() {
        createSequence();
        isResponding = false;
        elementToPlay = 0;
        playerResponses = 0;
        m_go.setText("Смотри и запоминай!");
        playSequence = true;
        myHandler.sendEmptyMessage(0);
    }

    public void sequenceFinished() {
        playSequence = false;
        m_go.setText("Угадай!");
        isResponding = true;
    }

    public void checkElement(int thisElement) {
        if (isResponding) {
            playerResponses++;
            if (sequenceToCopy[playerResponses - 1] == thisElement) {
                playerScore = playerScore + ((thisElement + 1) * 2);
                m_score.setText("Результат: " + playerScore);
                if (playerResponses == difficultyLevel) {
                    isResponding = false;
                    difficultyLevel++;
                    playASequence();
                }
            } else {
                m_go.setText("Неверно!");
                isResponding = false;

                if(playerScore > hiScore) {
                    hiScore = playerScore;
                    editor.putInt(intName, hiScore);
                    editor.commit();
                    Toast.makeText(getApplicationContext(), "Новый Рекорд", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
